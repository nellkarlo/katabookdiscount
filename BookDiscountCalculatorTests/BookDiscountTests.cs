using BookStoreDiscount;
using System;
using Xunit;

namespace BookDiscountCalculatorTests
{
    public class BookDiscountTests
    {
        [Theory]
        [InlineData(0, new int[]{})]
        [InlineData(8, new int[]{1})]
        [InlineData(8, new int[]{2})]
        [InlineData(8, new int[]{3})]
        [InlineData(8 * 3, new int[]{1,1,1})]
        [InlineData(8 * 2 * 0.95, new int[]{0,1})]
        [InlineData(8 * 3 * 0.9, new int[]{0,2,3})]
        [InlineData(8 * 4 * 0.8, new int[]{0,1,2,3})]
        [InlineData(8 + (8 * 2 * 0.95), new int[]{0,0,1})]
        [InlineData(2 * (8 * 2 * 0.95), new int[]{0,0,1,1 })]
        [InlineData((8 * 4 * 0.8) + (8 * 2 * 0.95), new int[]{ 0, 0, 1, 2, 2, 3 })]
        public void BookDiscountCalculator_GivenAnIntArrayOfBooks_ReturnsProperDiscount(double expected, int[] books)
        {
            //Arrange
            double actual;

            //Act
            actual = Program.BookDiscountCalculator(books);

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
